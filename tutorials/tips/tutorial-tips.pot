msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-11-01 13:46+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Put one translator per line, in the form NAME <EMAIL>, YEAR1, YEAR2
msgctxt "_"
msgid "translator-credits"
msgstr ""

#. (itstool) path: articleinfo/title
#: tutorial-tips.xml:6
msgid "Tips and Tricks"
msgstr ""

#. (itstool) path: articleinfo/subtitle
#: tutorial-tips.xml:7
msgid "Tutorial"
msgstr ""

#. (itstool) path: abstract/para
#: tutorial-tips.xml:11
msgid ""
"This tutorial will demonstrate various tips and tricks that users have "
"learned through the use of Inkscape and some “hidden” features that can help "
"you speed up production tasks."
msgstr ""

#. (itstool) path: sect1/title
#: tutorial-tips.xml:17
msgid "Radial placement with Tiled Clones"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-tips.xml:18
msgid ""
"It's easy to see how to use the <guimenuitem>Create Tiled Clones</"
"guimenuitem> dialog for rectangular grids and patterns. But what if you need "
"<firstterm>radial</firstterm> placement, where objects share a common center "
"of rotation? It's possible too!"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-tips.xml:23
msgid ""
"If your radial pattern only needs to have 3, 4, 6, 8, or 12 elements, then "
"you can try the P3, P31M, P3M1, P4, P4M, P6, or P6M symmetries. These will "
"work nicely for snowflakes and the like. A more general method, however, is "
"as follows."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-tips.xml:28
msgid ""
"Choose the P1 symmetry (simple translation) and then <emphasis>compensate</"
"emphasis> for that translation by going to the <guimenuitem>Shift</"
"guimenuitem> tab and setting <guilabel>Per row/Shift Y</guilabel> and "
"<guilabel>Per column/Shift X</guilabel> both to -100%. Now all clones will "
"be stacked exactly on top of the original. All that remains to do is to go "
"to the <guimenuitem>Rotation</guimenuitem> tab and set some rotation angle "
"per column, then create the pattern with one row and multiple columns. For "
"example, here's a pattern made out of a horizontal line, with 30 columns, "
"each column rotated 6 degrees:"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-tips.xml:43
msgid ""
"To get a clock dial out of this, all you need to do is cut out or simply "
"overlay the central part by a white circle (to do boolean operations on "
"multiple clones at once, combine them first)."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-tips.xml:47
msgid ""
"More interesting effects can be created by using both rows and columns. "
"Here's a pattern with 10 columns and 8 rows, with rotation of 2 degrees per "
"row and 18 degrees per column. Each group of lines here is a “column”, so "
"the groups are 18 degrees from each other; within each column, individual "
"lines are 2 degrees apart:"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-tips.xml:59
msgid ""
"In the above examples, the line was rotated around its center. But what if "
"you want the center to be outside of your shape? Just click on the object "
"twice with the Selector tool to enter rotation mode. Now move the object's "
"rotation center (represented by a small cross-shaped handle) to the point "
"you would like to be the center of the rotation for the Tiled Clones "
"operation. Then use <guimenuitem>Create Tiled Clones</guimenuitem> on the "
"object. This is how you can do nice “explosions” or “starbursts” by "
"randomizing scale, rotation, and possibly opacity:"
msgstr ""

#. (itstool) path: sect1/title
#: tutorial-tips.xml:76
msgid "Non-linear gradients"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-tips.xml:77
msgid ""
"The version 1.1 of SVG does not support non-linear gradients (i.e. those "
"which have a non-linear translations between colors). You can, however, "
"emulate them by <firstterm>multistop</firstterm> gradients."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-tips.xml:81
msgid ""
"Start with a simple two-stop gradient (you can assign that in the Fill and "
"Stroke dialog or use the gradient tool). Now, with the gradient tool, add a "
"new gradient stop in the middle; either by double-clicking on the gradient "
"line, or by selecting the square-shaped gradient stop and clicking on the "
"button <guimenuitem>Insert new stop</guimenuitem> in the gradient tool's "
"tool bar at the top. Drag the new stop a bit. Then add more stops before and "
"after the middle stop and drag them too, so that the gradient looks smooth. "
"The more stops you add, the smoother you can make the resulting gradient. "
"Here's the initial black-white gradient with two stops:"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-tips.xml:96
msgid ""
"And here are various “non-linear” multi-stop gradients (examine them in the "
"Gradient Editor):"
msgstr ""

#. (itstool) path: sect1/title
#: tutorial-tips.xml:108
msgid "Excentric radial gradients"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-tips.xml:109
msgid ""
"Radial gradients don't have to be symmetric. In Gradient tool, drag the "
"central handle of an elliptic gradient with <keycap "
"function=\"shift\">Shift</keycap>. This will move the x-shaped "
"<firstterm>focus handle</firstterm> of the gradient away from its center. "
"When you don't need it, you can snap the focus back by dragging it close to "
"the center."
msgstr ""

#. (itstool) path: sect1/title
#: tutorial-tips.xml:124
msgid "Mesh gradients"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-tips.xml:125
msgid ""
"For even more complex gradients, Inkscape offers the <firstterm>Mesh "
"Gradient tool</firstterm> (the tool right below the gradient tool in the "
"tool bar). Set the shape (radial or grid) and the number of rows and columns "
"of color patches in the tool controls bar, and then use the tool to drag "
"over any shape to fill it with a mesh gradient."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-tips.xml:131
msgid ""
"Now you can adjust the shape of the color patches with the triangular "
"handles, and select the diamond-shaped handles to change the patches' color. "
"You can even pick the color of objects below the mesh gradient by clicking "
"on the color picker icon in its tool controls."
msgstr ""

#. (itstool) path: sect1/title
#: tutorial-tips.xml:138
msgid "Aligning to the center of the page"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-tips.xml:139
msgid ""
"To align something to the center or side of a page, select the object or "
"group and then choose <guimenuitem>Page</guimenuitem> from the "
"<guilabel>Relative to:</guilabel> list in the <guimenuitem>Align and "
"Distribute</guimenuitem> dialog (<keycombo><keycap function=\"shift\">Shift</"
"keycap><keycap function=\"control\">Ctrl</keycap><keycap>A</keycap></"
"keycombo>)."
msgstr ""

#. (itstool) path: sect1/title
#: tutorial-tips.xml:147
msgid "Cleaning up the document"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-tips.xml:148
msgid ""
"Many of the no-longer-used gradients, patterns, and markers (more precisely, "
"those which you edited manually) remain in the corresponding palettes and "
"can be reused for new objects. However if you want to optimize your "
"document, use the <guimenuitem>Clean Up Document</guimenuitem> command in "
"<guimenu>File</guimenu> menu. It will remove any gradients, patterns, or "
"markers which are not used by anything in the document, making the file "
"smaller."
msgstr ""

#. (itstool) path: sect1/title
#: tutorial-tips.xml:157
msgid "Hidden features and the XML editor"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-tips.xml:158
msgid ""
"The XML editor (<keycombo><keycap function=\"shift\">Shift</keycap><keycap "
"function=\"control\">Ctrl</keycap><keycap>X</keycap></keycombo>) allows you "
"to change almost all aspects of the document without using an external text "
"editor. Also, Inkscape usually supports more SVG features than are "
"accessible from the GUI. The XML editor is one way to get access to these "
"features (if you know SVG)."
msgstr ""

#. (itstool) path: sect1/title
#: tutorial-tips.xml:167
msgid "Changing the rulers' unit of measure"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-tips.xml:168
msgid ""
"In the default template, the unit of measure used by the rulers is mm. This "
"is also the unit used in displaying coordinates at the lower-right corner "
"and preselected in all units menus. (You can always hover your "
"<mousebutton>mouse</mousebutton> over a ruler to see the tooltip with the "
"units it uses.) To change this, open <guimenuitem>Document Properties</"
"guimenuitem> (<keycombo><keycap function=\"shift\">Shift</keycap><keycap "
"function=\"control\">Ctrl</keycap><keycap>D</keycap></keycombo>) and change "
"the <guimenuitem>Display units</guimenuitem> on the <guimenuitem>Display</"
"guimenuitem> tab."
msgstr ""

#. (itstool) path: sect1/title
#: tutorial-tips.xml:178
msgid "Stamping"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-tips.xml:179
msgid ""
"To quickly create many copies of an object, use <firstterm>stamping</"
"firstterm>. Just drag an object (or scale or rotate it), and while holding "
"the <mousebutton role=\"click\">mouse</mousebutton> button down, press "
"<keycap>Space</keycap>. This leaves a “stamp” of the current object shape. "
"You can repeat it as many times as you wish."
msgstr ""

#. (itstool) path: sect1/title
#: tutorial-tips.xml:187
msgid "Pen tool tricks"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-tips.xml:188
msgid ""
"In the Pen (Bezier) tool, you have the following options to finish the "
"current line:"
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-tips.xml:193
msgid "Press <keycap>Enter</keycap>"
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-tips.xml:198
msgid ""
"<mousebutton role=\"double-click\">Double click</mousebutton> with the left "
"mouse button"
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-tips.xml:203
msgid ""
"Click with the <mousebutton role=\"right-click\">right</mousebutton> mouse "
"button"
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-tips.xml:208
msgid "Select another tool"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-tips.xml:213
msgid ""
"Note that while the path is unfinished (i.e. is shown green, with the "
"current segment red) it does not yet exist as an object in the document. "
"Therefore, to cancel it, use either <keycap>Esc</keycap> (cancel the whole "
"path) or <keycap>Backspace</keycap> (remove the last segment of the "
"unfinished path) instead of <guimenuitem>Undo</guimenuitem>."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-tips.xml:219
msgid ""
"To add a new subpath to an existing path, select that path and start drawing "
"with <keycap function=\"shift\">Shift</keycap> from an arbitrary point. If, "
"however, what you want is to simply <emphasis>continue</emphasis> an "
"existing path, Shift is not necessary; just start drawing from one of the "
"end anchors of the selected path."
msgstr ""

#. (itstool) path: sect1/title
#: tutorial-tips.xml:227
msgid "Entering Unicode values"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-tips.xml:228
msgid ""
"While in the Text tool, pressing <keycombo><keycap "
"function=\"control\">Ctrl</keycap><keycap>U</keycap></keycombo> toggles "
"between Unicode and normal mode. In Unicode mode, each group of 4 "
"hexadecimal digits you type becomes a single Unicode character, thus "
"allowing you to enter arbitrary symbols (as long as you know their Unicode "
"codepoints and the font supports them). To finish the Unicode input, press "
"<keycap>Enter</keycap>. For example, <keycombo action=\"seq\"><keycap "
"function=\"control\">Ctrl</keycap><keycap>U</keycap><keycap>2</"
"keycap><keycap>0</keycap><keycap>1</keycap><keycap>4</keycap><keycap>Enter</"
"keycap></keycombo> inserts an em-dash (—). To quit the Unicode mode without "
"inserting anything press <keycap>Esc</keycap>."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-tips.xml:236
msgid ""
"You can also use the <menuchoice><guimenu>Text</guimenu><guimenuitem>Unicode "
"Characters</guimenuitem></menuchoice> dialog to search for and insert glyphs "
"into your document."
msgstr ""

#. (itstool) path: sect1/title
#: tutorial-tips.xml:242
msgid "Using the grid for drawing icons"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-tips.xml:243
msgid ""
"Suppose you want to create a 24x24 pixel icon. Create a 24x24 px canvas (use "
"the <guimenuitem>Document Preferences</guimenuitem>) and set the grid to 0.5 "
"px (48x48 gridlines). Now, if you align filled objects to <emphasis>even</"
"emphasis> gridlines, and stroked objects to <emphasis>odd</emphasis> "
"gridlines with the stroke width in px being an even number, and export it at "
"the default 96dpi (so that 1 px becomes 1 bitmap pixel), you get a crisp "
"bitmap image without unneeded antialiasing."
msgstr ""

#. (itstool) path: sect1/title
#: tutorial-tips.xml:252
msgid "Object rotation"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-tips.xml:253
msgid ""
"When in the Selector tool, <mousebutton role=\"click\">click</mousebutton> "
"on an object to see the scaling arrows, then <mousebutton "
"role=\"click\">click</mousebutton> again on the object to see the rotation "
"and skew arrows. If the arrows at the corners are clicked and dragged, the "
"object will rotate around the center (shown as a cross mark). If you hold "
"down the <keycap function=\"shift\">Shift</keycap> key while doing this, the "
"rotation will occur around the opposite corner. You can also drag the "
"rotation center to any place."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-tips.xml:260
msgid ""
"Or, you can rotate from keyboard by pressing <keycap>[</keycap> and "
"<keycap>]</keycap> (by 15 degrees) or <keycombo><keycap "
"function=\"control\">Ctrl</keycap><keycap>[</keycap></keycombo> and "
"<keycombo><keycap function=\"control\">Ctrl</keycap><keycap>]</keycap></"
"keycombo> (by 90 degrees). The same <keycap>[</keycap> <keycap>]</keycap> "
"keys with <keycap function=\"alt\">Alt</keycap> perform slow pixel-size "
"rotation."
msgstr ""

#. (itstool) path: sect1/title
#: tutorial-tips.xml:269
msgid "On-Canvas Alignment"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-tips.xml:270
msgid ""
"When the option <guilabel>Enable on-canvas alignment</guilabel> is active in "
"the <guimenuitem>Align and Distribute</guimenuitem> dialog, select some "
"objects and slowly <mousebutton role=\"click\">click</mousebutton> twice on "
"them to enable handles for on canvas alignment. The handles can be used to "
"align the selected objects relative to the area of the current selection."
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-tips.xml:278
msgid ""
"<mousebutton role=\"click\">Click</mousebutton> on handles will align "
"objects relative to the selection area."
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-tips.xml:283
msgid ""
"<mousebutton role=\"click\">Click</mousebutton> on the central handle to "
"align the selected objects on the horizontal axis. <keycombo><keycap "
"function=\"shift\">Shift</keycap><mousebutton role=\"click\">click</"
"mousebutton></keycombo> on objects will aligned them on the vertical axis."
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-tips.xml:292
msgid ""
"<keycombo><keycap function=\"shift\">Shift</keycap><mousebutton "
"role=\"click\">click</mousebutton></keycombo> on the outer handles aligns on "
"the outside of the selection area."
msgstr ""

#. (itstool) path: sect1/title
#: tutorial-tips.xml:300
msgid "Drop shadows"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-tips.xml:301
msgid ""
"To quickly create drop shadows for objects, use the "
"<menuchoice><guimenu>Filters</guimenu><guisubmenu>Shadows and Glows</"
"guisubmenu><guimenuitem>Drop Shadow</guimenuitem></menuchoice> feature."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-tips.xml:305
msgid ""
"You can also easily create blurred drop shadows for objects manually with "
"blur in the Fill and Stroke dialog. Select an object, duplicate it by "
"<keycombo><keycap function=\"control\">Ctrl</keycap><keycap>D</keycap></"
"keycombo>, press <keycap>PgDown</keycap> to put it beneath original object, "
"place it a little to the right and lower than original object. Now open Fill "
"And Stroke dialog and change Blur value to, say, 5.0. That's it!"
msgstr ""

#. (itstool) path: sect1/title
#: tutorial-tips.xml:314
msgid "Placing text on a path"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-tips.xml:315
msgid ""
"To place text along a curve, select the text and the curve together and "
"choose <guimenuitem>Put on Path</guimenuitem> from the <guimenu>Text</"
"guimenu> menu. The text will start at the beginning of the path. In general "
"it is best to create an explicit path that you want the text to be fitted "
"to, rather than fitting it to some other drawing element — this will give "
"you more control without screwing over your drawing."
msgstr ""

#. (itstool) path: sect1/title
#: tutorial-tips.xml:323
msgid "Selecting the original"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-tips.xml:324
msgid ""
"When you have a text on path, a linked offset, or a clone, their source "
"object/path may be difficult to select because it may be directly "
"underneath, or made invisible and/or locked. The magic key <keycombo><keycap "
"function=\"shift\">Shift</keycap><keycap>D</keycap></keycombo> will help "
"you; select the text, linked offset, or clone, and press <keycombo><keycap "
"function=\"shift\">Shift</keycap><keycap>D</keycap></keycombo> to move "
"selection to the corresponding path, offset source, or clone original."
msgstr ""

#. (itstool) path: sect1/title
#: tutorial-tips.xml:334
msgid "Window off-screen recovery"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-tips.xml:335
msgid ""
"When moving documents between systems with different resolutions or number "
"of displays, you may find Inkscape has saved a window position that places "
"the window out of reach on your screen. Simply maximise the window (which "
"will bring it back into view, use the task bar), save and reload. You can "
"avoid this altogether by unchecking the global option to save window "
"geometry (<guimenuitem>Inkscape Preferences</guimenuitem>, "
"<menuchoice><guimenu>Interface</guimenu><guimenuitem>Windows</guimenuitem></"
"menuchoice> section)."
msgstr ""

#. (itstool) path: sect1/title
#: tutorial-tips.xml:344
msgid "Transparency, gradients, and PostScript export"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-tips.xml:345
msgid ""
"PostScript or EPS formats do not support <emphasis>transparency</emphasis>, "
"so you should never use it if you are going to export to PS/EPS. In the case "
"of flat transparency which overlays flat color, it's easy to fix it: Select "
"one of the transparent objects; switch to the Dropper tool (<keycap>F7</"
"keycap> or <keycap>D</keycap>); make sure that the <guilabel>Opacity: Pick</"
"guilabel> button in the dropper tool's tool bar is deactivated; click on "
"that same object. That will pick the visible color and assign it back to the "
"object, but this time without transparency. Repeat for all transparent "
"objects. If your transparent object overlays several flat color areas, you "
"will need to break it correspondingly into pieces and apply this procedure "
"to each piece. Note that the dropper tool does not change the opacity value "
"of the object, but only the alpha value of its fill or stroke color, so make "
"sure that every object's opacity value is set to 100% before you start out."
msgstr ""

#. (itstool) path: sect1/title
#: tutorial-tips.xml:358
msgid "Interactivity"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-tips.xml:359
msgid ""
"Most SVG elements can be tweaked to react to user input (usually this will "
"only work if the SVG is displayed in a web browser)."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-tips.xml:363
msgid ""
"The simplest possibility is to add a clickable link to objects. For this "
"<mousebutton role=\"right-click\">right-click</mousebutton> the object and "
"select <menuchoice><guimenuitem>Create Link</guimenuitem></menuchoice> from "
"the context menu. The \"Object attributes\" dialog will open, where you can "
"set the target of the link using the value of <guilabel>href</guilabel>."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-tips.xml:369
msgid ""
"More control is possible using the interactivity attributes accessible from "
"the \"Object Properties\" dialog (<keycombo><keycap "
"function=\"control\">Ctrl</keycap><keycap function=\"shift\">Shift</"
"keycap><keycap>O</keycap></keycombo>). Here you can implement arbitrary "
"functionality using JavaScript. Some basic examples:"
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-tips.xml:376
msgid "Open another file in the current window when clicking on the object:"
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-tips.xml:381
msgid ""
"Set <guilabel>onclick</guilabel> to <code>window.location='file2.svg';</code>"
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-tips.xml:388
msgid "Open an arbitrary weblink in new window when clicking on the object:"
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-tips.xml:393
msgid ""
"Set <guilabel>onclick</guilabel> to <code>window.open(\"https://inkscape."
"org\",\"_blank\");</code>"
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-tips.xml:400
msgid "Reduce transparency of the object while hovering:"
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-tips.xml:405
msgid ""
"Set <guilabel>onmouseover</guilabel> to <code>style.opacity = 0.5;</code>"
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-tips.xml:410
msgid "Set <guilabel>onmouseout</guilabel> to <code>style.opacity = 1;</code>"
msgstr ""

#. (itstool) path: Work/format
#: tips-f01.svg:49 tips-f02.svg:49 tips-f03.svg:49 tips-f04.svg:70
#: tips-f05.svg:558 tips-f06.svg:136
msgid "image/svg+xml"
msgstr ""
