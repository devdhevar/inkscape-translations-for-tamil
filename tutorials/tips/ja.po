msgid ""
msgstr ""
"Project-Id-Version: Inkscape tutorial: Tips and Tricks\n"
"POT-Creation-Date: 2023-11-01 13:46+0100\n"
"PO-Revision-Date: 2012-03-20 19:44+0900\n"
"Last-Translator: Masato Hashimoto <cabezon.hashimoto@gmail.com>\n"
"Language-Team: Japanese <inkscape-translator@lists.sourceforge.net>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-Language: Japanese\n"

#. Put one translator per line, in the form NAME <EMAIL>, YEAR1, YEAR2
msgctxt "_"
msgid "translator-credits"
msgstr "Masato Hashimoto <cabezon.hashimoto@gmail.com>, 2009, 2010, 2012"

#. (itstool) path: articleinfo/title
#: tutorial-tips.xml:6
msgid "Tips and Tricks"
msgstr "ヒントやコツ"

#. (itstool) path: articleinfo/subtitle
#: tutorial-tips.xml:7
msgid "Tutorial"
msgstr ""

#. (itstool) path: abstract/para
#: tutorial-tips.xml:11
msgid ""
"This tutorial will demonstrate various tips and tricks that users have "
"learned through the use of Inkscape and some “hidden” features that can help "
"you speed up production tasks."
msgstr ""
"このチュートリアルは、作品制作を助け効率化を図るいくつかの「隠し機能」やユー"
"ザーが見つけたさまざまなヒントやコツを紹介しています。"

#. (itstool) path: sect1/title
#: tutorial-tips.xml:17
#, fuzzy
msgid "Radial placement with Tiled Clones"
msgstr "タイルクローンを使った放射状の配置"

#. (itstool) path: sect1/para
#: tutorial-tips.xml:18
#, fuzzy
msgid ""
"It's easy to see how to use the <guimenuitem>Create Tiled Clones</"
"guimenuitem> dialog for rectangular grids and patterns. But what if you need "
"<firstterm>radial</firstterm> placement, where objects share a common center "
"of rotation? It's possible too!"
msgstr ""
"直角のグリッドやパターンは <command>タイルクローン</command> で簡単に作成でき"
"ます。では、共通の中心から回転させる <firstterm>放射状</firstterm> の配置とか"
"はどうでしょう? それもできます!"

#. (itstool) path: sect1/para
#: tutorial-tips.xml:23
#, fuzzy
msgid ""
"If your radial pattern only needs to have 3, 4, 6, 8, or 12 elements, then "
"you can try the P3, P31M, P3M1, P4, P4M, P6, or P6M symmetries. These will "
"work nicely for snowflakes and the like. A more general method, however, is "
"as follows."
msgstr ""
"作りたい放射状パターンが、3、4、6、8、あるいは 12 の要素だけでいいなら、対称"
"化の P3、P31M、P3M1、P4、P4M、P6、あるいは P6M を試してみてください。これは雪"
"片などを作成するのに適しています。しかし、もっと汎用的なやり方だと次のように"
"なります。"

#. (itstool) path: sect1/para
#: tutorial-tips.xml:28
#, fuzzy
msgid ""
"Choose the P1 symmetry (simple translation) and then <emphasis>compensate</"
"emphasis> for that translation by going to the <guimenuitem>Shift</"
"guimenuitem> tab and setting <guilabel>Per row/Shift Y</guilabel> and "
"<guilabel>Per column/Shift X</guilabel> both to -100%. Now all clones will "
"be stacked exactly on top of the original. All that remains to do is to go "
"to the <guimenuitem>Rotation</guimenuitem> tab and set some rotation angle "
"per column, then create the pattern with one row and multiple columns. For "
"example, here's a pattern made out of a horizontal line, with 30 columns, "
"each column rotated 6 degrees:"
msgstr ""
"対称化の P1 (シンプル移動) を選び、<command>シフト</command>  タブで "
"<command>行ごと/垂直シフト</command> および <command>列ごと/水平シフト</"
"command> をそれぞれ -100% に設定し、移動を<emphasis>平衡</emphasis>させます "
"(訳注: 「タイルを考慮しない」を選んでもできます)。これですべてのクローンはオ"
"リジナルの上に正確に積み重なります。あとは <command>回転</command> タブで列ご"
"との回転角度を設定し、行が 1、列が複数になるようにして作成します。例として、"
"以下に水平線、列数 30、回転角度 6°で作成したパターンを示します。"

#. (itstool) path: sect1/para
#: tutorial-tips.xml:43
#, fuzzy
msgid ""
"To get a clock dial out of this, all you need to do is cut out or simply "
"overlay the central part by a white circle (to do boolean operations on "
"multiple clones at once, combine them first)."
msgstr ""
"これを時計の目盛り板のようにするには、切り抜くか、シンプルに白い円で中心部を"
"覆い隠します (クローン上でブーリアン演算を行うには、まずこれらのリンクを解除"
"してください)。"

#. (itstool) path: sect1/para
#: tutorial-tips.xml:47
msgid ""
"More interesting effects can be created by using both rows and columns. "
"Here's a pattern with 10 columns and 8 rows, with rotation of 2 degrees per "
"row and 18 degrees per column. Each group of lines here is a “column”, so "
"the groups are 18 degrees from each other; within each column, individual "
"lines are 2 degrees apart:"
msgstr ""
"行と列の両方を使うことによって、もっとおもしろい効果を作り出せます。ここに挙"
"げたパターンは、列数 10、行数 8、行ごとの回転角度 2°、列ごとの回転角度 18°で"
"作成したものです。ここでの各線のグループが「列」であり、グループはそれぞれ "
"18°ずつ離れています。各列内では、個々の線は 2°ずつ離れています:"

#. (itstool) path: sect1/para
#: tutorial-tips.xml:59
#, fuzzy
msgid ""
"In the above examples, the line was rotated around its center. But what if "
"you want the center to be outside of your shape? Just click on the object "
"twice with the Selector tool to enter rotation mode. Now move the object's "
"rotation center (represented by a small cross-shaped handle) to the point "
"you would like to be the center of the rotation for the Tiled Clones "
"operation. Then use <guimenuitem>Create Tiled Clones</guimenuitem> on the "
"object. This is how you can do nice “explosions” or “starbursts” by "
"randomizing scale, rotation, and possibly opacity:"
msgstr ""
"上の例では線は中心から回転しています。しかし、中心をシェイプの外に置きたい時"
"はどうしたらよいでしょう? そのシェイプを覆うように、中心を置きたいところにし"
"た見えない矩形 (フィルなし、ストロークなし) を作成し、シェイプと矩形をグルー"
"プ化し、そのグループ上で <command>タイルクローン</command> を使用します。これ"
"は、拡大縮小、回転、そしておそらく不透明度もランダム化することでナイスな「爆"
"発」あるいは「星形」にすることができる方法です:"

#. (itstool) path: sect1/title
#: tutorial-tips.xml:76
msgid "Non-linear gradients"
msgstr "非線形グラデーション"

#. (itstool) path: sect1/para
#: tutorial-tips.xml:77
msgid ""
"The version 1.1 of SVG does not support non-linear gradients (i.e. those "
"which have a non-linear translations between colors). You can, however, "
"emulate them by <firstterm>multistop</firstterm> gradients."
msgstr ""
"SVG バージョン 1.1 は非線形グラデーション (すなわち色の非線形変換) をサポート"
"していません。しかし、<firstterm>マルチストップ</firstterm> グラデーションに"
"よってエミュレートすることができます。"

#. (itstool) path: sect1/para
#: tutorial-tips.xml:81
#, fuzzy
msgid ""
"Start with a simple two-stop gradient (you can assign that in the Fill and "
"Stroke dialog or use the gradient tool). Now, with the gradient tool, add a "
"new gradient stop in the middle; either by double-clicking on the gradient "
"line, or by selecting the square-shaped gradient stop and clicking on the "
"button <guimenuitem>Insert new stop</guimenuitem> in the gradient tool's "
"tool bar at the top. Drag the new stop a bit. Then add more stops before and "
"after the middle stop and drag them too, so that the gradient looks smooth. "
"The more stops you add, the smoother you can make the resulting gradient. "
"Here's the initial black-white gradient with two stops:"
msgstr ""
"まず、シンプルな 2 停止点のグラデーションを作成します。グラデーションエディ"
"ターを開き (グラデーションツールでグラデーションハンドルをダブルクリック)、中"
"間に新しい色フェーズを追加します (すこしドラッグしてください)。さらに色フェー"
"ズを追加し、それらもドラッグすることでグラデーションは滑らかになります。そし"
"てさらに色フェーズを追加すれば、よりなめらかなグラデーションができあがりま"
"す。以下は最初の白と黒の 2 停止点のみのグラデーションです:"

#. (itstool) path: sect1/para
#: tutorial-tips.xml:96
msgid ""
"And here are various “non-linear” multi-stop gradients (examine them in the "
"Gradient Editor):"
msgstr ""
"そして以下はさまざまな「非線形」マルチストップグラデーションです (グラデー"
"ションエディターで調べてみてください)。"

#. (itstool) path: sect1/title
#: tutorial-tips.xml:108
msgid "Excentric radial gradients"
msgstr "一風変わった放射状グラデーション"

#. (itstool) path: sect1/para
#: tutorial-tips.xml:109
#, fuzzy
msgid ""
"Radial gradients don't have to be symmetric. In Gradient tool, drag the "
"central handle of an elliptic gradient with <keycap "
"function=\"shift\">Shift</keycap>. This will move the x-shaped "
"<firstterm>focus handle</firstterm> of the gradient away from its center. "
"When you don't need it, you can snap the focus back by dragging it close to "
"the center."
msgstr ""
"放射状グラデーションは対称性を持ちません。グラデーションツールで、楕円グラ"
"デーションの中心ハンドルを <keycap>Shift</keycap> キーを押しながらドラッグす"
"ると、×字形のグラデーションの<firstterm>焦点ハンドル</firstterm>が中心を離れ"
"て移動します。もし焦点を中心と同じ位置に戻したい場合は、中心ハンドル付近にド"
"ラッグすれば中心にスナップされます。"

#. (itstool) path: sect1/title
#: tutorial-tips.xml:124
#, fuzzy
msgid "Mesh gradients"
msgstr "非線形グラデーション"

#. (itstool) path: sect1/para
#: tutorial-tips.xml:125
msgid ""
"For even more complex gradients, Inkscape offers the <firstterm>Mesh "
"Gradient tool</firstterm> (the tool right below the gradient tool in the "
"tool bar). Set the shape (radial or grid) and the number of rows and columns "
"of color patches in the tool controls bar, and then use the tool to drag "
"over any shape to fill it with a mesh gradient."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-tips.xml:131
msgid ""
"Now you can adjust the shape of the color patches with the triangular "
"handles, and select the diamond-shaped handles to change the patches' color. "
"You can even pick the color of objects below the mesh gradient by clicking "
"on the color picker icon in its tool controls."
msgstr ""

#. (itstool) path: sect1/title
#: tutorial-tips.xml:138
msgid "Aligning to the center of the page"
msgstr "ページ中央への配置"

#. (itstool) path: sect1/para
#: tutorial-tips.xml:139
#, fuzzy
msgid ""
"To align something to the center or side of a page, select the object or "
"group and then choose <guimenuitem>Page</guimenuitem> from the "
"<guilabel>Relative to:</guilabel> list in the <guimenuitem>Align and "
"Distribute</guimenuitem> dialog (<keycombo><keycap function=\"shift\">Shift</"
"keycap><keycap function=\"control\">Ctrl</keycap><keycap>A</keycap></"
"keycombo>)."
msgstr ""
"オブジェクトやグループなどをページの中心あるいは端に配置するには、それを選択"
"し、「整列と配置」ダイアログ (<keycap>Ctrl+Shift+A</keycap>) で <command>基"
"準:</command> を <command>ページ</command> にして行います。"

#. (itstool) path: sect1/title
#: tutorial-tips.xml:147
msgid "Cleaning up the document"
msgstr "ドキュメントのクリーンアップ"

#. (itstool) path: sect1/para
#: tutorial-tips.xml:148
#, fuzzy
msgid ""
"Many of the no-longer-used gradients, patterns, and markers (more precisely, "
"those which you edited manually) remain in the corresponding palettes and "
"can be reused for new objects. However if you want to optimize your "
"document, use the <guimenuitem>Clean Up Document</guimenuitem> command in "
"<guimenu>File</guimenu> menu. It will remove any gradients, patterns, or "
"markers which are not used by anything in the document, making the file "
"smaller."
msgstr ""
"パレットにもはや使われていないたくさんのグラデーション、パターン、あるいは"
"マーカー (正確に言えば、あなたが編集したもの) が残っていても、それは新しいオ"
"ブジェクトに再利用することができます。しかし、ドキュメントにあわせて整理した"
"ければ、ファイルメニューの <command>Def のバキューム</command> コマンドを使っ"
"てください。これはそのドキュメントで使用されていないすべてのグラデーション、"
"パターン、あるいはマーカーを削除し、ファイルサイズを小さくします。"

#. (itstool) path: sect1/title
#: tutorial-tips.xml:157
msgid "Hidden features and the XML editor"
msgstr "XML エディターの隠し機能"

#. (itstool) path: sect1/para
#: tutorial-tips.xml:158
#, fuzzy
msgid ""
"The XML editor (<keycombo><keycap function=\"shift\">Shift</keycap><keycap "
"function=\"control\">Ctrl</keycap><keycap>X</keycap></keycombo>) allows you "
"to change almost all aspects of the document without using an external text "
"editor. Also, Inkscape usually supports more SVG features than are "
"accessible from the GUI. The XML editor is one way to get access to these "
"features (if you know SVG)."
msgstr ""
"XML エディター (<keycap>Shift+Ctrl+X</keycap>) では、外部のテキストエディター"
"を使用せずに、ドキュメントをほとんどすべての面から変更できます。Inkscape は "
"GUI から編集できるよりも多くの SVG 機能をサポートしていますが、XML エディター"
"はこれらの機能を利用する一つの手段です (もしあなたが SVG についてご存知なら"
"ば)。"

#. (itstool) path: sect1/title
#: tutorial-tips.xml:167
msgid "Changing the rulers' unit of measure"
msgstr "ルーラーの単位の変更"

#. (itstool) path: sect1/para
#: tutorial-tips.xml:168
#, fuzzy
msgid ""
"In the default template, the unit of measure used by the rulers is mm. This "
"is also the unit used in displaying coordinates at the lower-right corner "
"and preselected in all units menus. (You can always hover your "
"<mousebutton>mouse</mousebutton> over a ruler to see the tooltip with the "
"units it uses.) To change this, open <guimenuitem>Document Properties</"
"guimenuitem> (<keycombo><keycap function=\"shift\">Shift</keycap><keycap "
"function=\"control\">Ctrl</keycap><keycap>D</keycap></keycombo>) and change "
"the <guimenuitem>Display units</guimenuitem> on the <guimenuitem>Display</"
"guimenuitem> tab."
msgstr ""
"デフォルトテンプレートでは、ルーラーで使用される単位は px (「SVG ユーザー単"
"位」、Inkscape では 0.8pt または 1/90 インチ) です。これは座標の表示や単位メ"
"ニューの初期値に使われています (マウスポインターをルーラーの上に移動させると"
"使用している単位がツールチップに表示されます)。これを変更するには、<command>"
"ドキュメントの設定</command> (<keycap>Ctrl+Shift+D</keycap>) を開き、"
"<command>ページ</command> タブの <command>デフォルト単位</command> を変更しま"
"す。"

#. (itstool) path: sect1/title
#: tutorial-tips.xml:178
msgid "Stamping"
msgstr "スタンプ"

#. (itstool) path: sect1/para
#: tutorial-tips.xml:179
#, fuzzy
msgid ""
"To quickly create many copies of an object, use <firstterm>stamping</"
"firstterm>. Just drag an object (or scale or rotate it), and while holding "
"the <mousebutton role=\"click\">mouse</mousebutton> button down, press "
"<keycap>Space</keycap>. This leaves a “stamp” of the current object shape. "
"You can repeat it as many times as you wish."
msgstr ""
"オブジェクトの複製を素早く作成するには、<firstterm>スタンプ</firstterm> 機能"
"を使います。オブジェクトをドラッグ (または拡大縮小や回転) し、マウスボタンを"
"押している間に <keycap>スペース</keycap> キーを押します。その時点でのオブジェ"
"クトが「スタンプ」として残ります。好きなだけこれを繰り返せます。"

#. (itstool) path: sect1/title
#: tutorial-tips.xml:187
msgid "Pen tool tricks"
msgstr "ペンツールの裏技"

#. (itstool) path: sect1/para
#: tutorial-tips.xml:188
msgid ""
"In the Pen (Bezier) tool, you have the following options to finish the "
"current line:"
msgstr ""
"ペン (ベジエ) ツールで描画中の線を完了させるには以下のオプションがあります:"

#. (itstool) path: listitem/para
#: tutorial-tips.xml:193
msgid "Press <keycap>Enter</keycap>"
msgstr "<keycap>Enter</keycap> キーを押す"

#. (itstool) path: listitem/para
#: tutorial-tips.xml:198
#, fuzzy
msgid ""
"<mousebutton role=\"double-click\">Double click</mousebutton> with the left "
"mouse button"
msgstr "マウスの左ボタンでダブルクリックする"

#. (itstool) path: listitem/para
#: tutorial-tips.xml:203
msgid ""
"Click with the <mousebutton role=\"right-click\">right</mousebutton> mouse "
"button"
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-tips.xml:208
msgid "Select another tool"
msgstr "他のツールを選択する"

#. (itstool) path: sect1/para
#: tutorial-tips.xml:213
#, fuzzy
msgid ""
"Note that while the path is unfinished (i.e. is shown green, with the "
"current segment red) it does not yet exist as an object in the document. "
"Therefore, to cancel it, use either <keycap>Esc</keycap> (cancel the whole "
"path) or <keycap>Backspace</keycap> (remove the last segment of the "
"unfinished path) instead of <guimenuitem>Undo</guimenuitem>."
msgstr ""
"パスが完了していない間 (描画した線が緑色、現在のセグメントは赤色) は、オブ"
"ジェクトはまだドキュメント上に存在していないことに注意してください。従って、"
"それをキャンセルするには、<command>元に戻す</command> のではなく、"
"<keycap>Esc</keycap> キー (パス全体をキャンセル) または <keycap>Backspace</"
"keycap> キー (未完了のパスの最後のセグメントのみ削除) を使用します。"

#. (itstool) path: sect1/para
#: tutorial-tips.xml:219
#, fuzzy
msgid ""
"To add a new subpath to an existing path, select that path and start drawing "
"with <keycap function=\"shift\">Shift</keycap> from an arbitrary point. If, "
"however, what you want is to simply <emphasis>continue</emphasis> an "
"existing path, Shift is not necessary; just start drawing from one of the "
"end anchors of the selected path."
msgstr ""
"既存のパスに新規にサブパスを追加するには、パスを選択し <keycap>Shift</"
"keycap> キーを押しながら任意のポイントを描画してください。しかし、既存のパス"
"を伸ばしたいだけならば、Shift を押す必要はありません。単に選択したパスの終点"
"から描画を開始してください。"

#. (itstool) path: sect1/title
#: tutorial-tips.xml:227
msgid "Entering Unicode values"
msgstr "Unicode 値の入力"

#. (itstool) path: sect1/para
#: tutorial-tips.xml:228
#, fuzzy
msgid ""
"While in the Text tool, pressing <keycombo><keycap "
"function=\"control\">Ctrl</keycap><keycap>U</keycap></keycombo> toggles "
"between Unicode and normal mode. In Unicode mode, each group of 4 "
"hexadecimal digits you type becomes a single Unicode character, thus "
"allowing you to enter arbitrary symbols (as long as you know their Unicode "
"codepoints and the font supports them). To finish the Unicode input, press "
"<keycap>Enter</keycap>. For example, <keycombo action=\"seq\"><keycap "
"function=\"control\">Ctrl</keycap><keycap>U</keycap><keycap>2</"
"keycap><keycap>0</keycap><keycap>1</keycap><keycap>4</keycap><keycap>Enter</"
"keycap></keycombo> inserts an em-dash (—). To quit the Unicode mode without "
"inserting anything press <keycap>Esc</keycap>."
msgstr ""
"テキストツールでは、<keycap>Ctrl+U</keycap> を押すことで Unicode と通常モード"
"を切り替えます。Unicode モードでは 4 個の 16 進数のグループを入力することで "
"Unicode 1 文字になりますので、(あなたがそれらの Unicode を知っており、フォン"
"トがサポートしている限り) 任意の記号を入力する事ができます。Unicode 入力を終"
"了するには <keycap>Enter</keycap> キーを押します。例えば、<keycap>Ctrl+U 2 0 "
"1 4 Enter</keycap> で emダッシュ(—) が挿入されます。何も入力せずに Unicode を"
"抜けるには <keycap>Esc</keycap> キーを押します。"

#. (itstool) path: sect1/para
#: tutorial-tips.xml:236
msgid ""
"You can also use the <menuchoice><guimenu>Text</guimenu><guimenuitem>Unicode "
"Characters</guimenuitem></menuchoice> dialog to search for and insert glyphs "
"into your document."
msgstr ""

#. (itstool) path: sect1/title
#: tutorial-tips.xml:242
msgid "Using the grid for drawing icons"
msgstr "アイコン描画のためのグリッドの使用"

#. (itstool) path: sect1/para
#: tutorial-tips.xml:243
#, fuzzy
msgid ""
"Suppose you want to create a 24x24 pixel icon. Create a 24x24 px canvas (use "
"the <guimenuitem>Document Preferences</guimenuitem>) and set the grid to 0.5 "
"px (48x48 gridlines). Now, if you align filled objects to <emphasis>even</"
"emphasis> gridlines, and stroked objects to <emphasis>odd</emphasis> "
"gridlines with the stroke width in px being an even number, and export it at "
"the default 96dpi (so that 1 px becomes 1 bitmap pixel), you get a crisp "
"bitmap image without unneeded antialiasing."
msgstr ""
"24×24 ピクセルのアイコンを作成したい場合は、24×24 px のキャンバスを作成 "
"(<command>ドキュメントの設定</command>) し、グリッドを 0.5 px (48×48 のグリッ"
"ド線に設定) します。フィルのオブジェクトを<emphasis>偶数</emphasis>のグリッド"
"線に、ストロークのオブジェクトを<emphasis>奇数</emphasis>のグリッド線に配置し"
"たら、それをデフォルトの 90dpi (1 px は 1 ビットマップピクセルになります) で"
"エクスポートします。するとアンチエイリアスのかかっていないビットマップ画像が"
"出来上がります。"

#. (itstool) path: sect1/title
#: tutorial-tips.xml:252
msgid "Object rotation"
msgstr "オブジェクトの回転"

#. (itstool) path: sect1/para
#: tutorial-tips.xml:253
#, fuzzy
msgid ""
"When in the Selector tool, <mousebutton role=\"click\">click</mousebutton> "
"on an object to see the scaling arrows, then <mousebutton "
"role=\"click\">click</mousebutton> again on the object to see the rotation "
"and skew arrows. If the arrows at the corners are clicked and dragged, the "
"object will rotate around the center (shown as a cross mark). If you hold "
"down the <keycap function=\"shift\">Shift</keycap> key while doing this, the "
"rotation will occur around the opposite corner. You can also drag the "
"rotation center to any place."
msgstr ""
"選択ツールの時に、オブジェクトを<keycap>クリック</keycap>すると、拡大縮小の矢"
"印が表示され、そのオブジェクト上で<keycap>再度クリック</keycap>すると回転およ"
"びシフトの矢印が表示されます。角の矢印をクリックおよびドラッグすると、オブ"
"ジェクトはその中心を軸 (＋印が表示されます) として回転します。<keycap>Shift</"
"keycap> キーを押すと、対角を軸として回転します。回転軸はドラッグで好きな位置"
"に置くことができます。"

#. (itstool) path: sect1/para
#: tutorial-tips.xml:260
#, fuzzy
msgid ""
"Or, you can rotate from keyboard by pressing <keycap>[</keycap> and "
"<keycap>]</keycap> (by 15 degrees) or <keycombo><keycap "
"function=\"control\">Ctrl</keycap><keycap>[</keycap></keycombo> and "
"<keycombo><keycap function=\"control\">Ctrl</keycap><keycap>]</keycap></"
"keycombo> (by 90 degrees). The same <keycap>[</keycap> <keycap>]</keycap> "
"keys with <keycap function=\"alt\">Alt</keycap> perform slow pixel-size "
"rotation."
msgstr ""
"また、キーボードから <keycap>[</keycap> および <keycap>]</keycap> キーを押す"
"と 15°ずつ回転し、<keycap>Ctrl+[</keycap> および <keycap>Ctrl+]</keycap> キー"
"を押すと90°ずつ回転します。<keycap>Alt</keycap> キーを押しながらだと、ピクセ"
"ルサイズ単位のゆっくりとした回転になります。"

#. (itstool) path: sect1/title
#: tutorial-tips.xml:269
msgid "On-Canvas Alignment"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-tips.xml:270
msgid ""
"When the option <guilabel>Enable on-canvas alignment</guilabel> is active in "
"the <guimenuitem>Align and Distribute</guimenuitem> dialog, select some "
"objects and slowly <mousebutton role=\"click\">click</mousebutton> twice on "
"them to enable handles for on canvas alignment. The handles can be used to "
"align the selected objects relative to the area of the current selection."
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-tips.xml:278
#, fuzzy
msgid ""
"<mousebutton role=\"click\">Click</mousebutton> on handles will align "
"objects relative to the selection area."
msgstr "マウスの左ボタンでダブルクリックする"

#. (itstool) path: listitem/para
#: tutorial-tips.xml:283
msgid ""
"<mousebutton role=\"click\">Click</mousebutton> on the central handle to "
"align the selected objects on the horizontal axis. <keycombo><keycap "
"function=\"shift\">Shift</keycap><mousebutton role=\"click\">click</"
"mousebutton></keycombo> on objects will aligned them on the vertical axis."
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-tips.xml:292
msgid ""
"<keycombo><keycap function=\"shift\">Shift</keycap><mousebutton "
"role=\"click\">click</mousebutton></keycombo> on the outer handles aligns on "
"the outside of the selection area."
msgstr ""

#. (itstool) path: sect1/title
#: tutorial-tips.xml:300
msgid "Drop shadows"
msgstr "影を落とす"

#. (itstool) path: sect1/para
#: tutorial-tips.xml:301
msgid ""
"To quickly create drop shadows for objects, use the "
"<menuchoice><guimenu>Filters</guimenu><guisubmenu>Shadows and Glows</"
"guisubmenu><guimenuitem>Drop Shadow</guimenuitem></menuchoice> feature."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-tips.xml:305
#, fuzzy
msgid ""
"You can also easily create blurred drop shadows for objects manually with "
"blur in the Fill and Stroke dialog. Select an object, duplicate it by "
"<keycombo><keycap function=\"control\">Ctrl</keycap><keycap>D</keycap></"
"keycombo>, press <keycap>PgDown</keycap> to put it beneath original object, "
"place it a little to the right and lower than original object. Now open Fill "
"And Stroke dialog and change Blur value to, say, 5.0. That's it!"
msgstr ""
"(訳注: Inkscape 0.47 以降では「フィルター」→「光と影」→「影を落とす」でできま"
"す) Inkscape はガウスぼかし SVG フィルターをサポートしていますので、オブジェ"
"クトにぼかした影を落とすのも簡単にできます。オブジェクトを選択し、"
"<keycap>Ctrl+D</keycap> で複製を作成し、<keycap>PgDown</keycap> でオリジナル"
"オブジェクトの真下に置き、それをオリジナルオブジェクトの斜め下にずらします。"
"フィル/ストロークダイアログを開いて「ぼかし」値を変更します。"

#. (itstool) path: sect1/title
#: tutorial-tips.xml:314
msgid "Placing text on a path"
msgstr "パス上にテキストを置く"

#. (itstool) path: sect1/para
#: tutorial-tips.xml:315
#, fuzzy
msgid ""
"To place text along a curve, select the text and the curve together and "
"choose <guimenuitem>Put on Path</guimenuitem> from the <guimenu>Text</"
"guimenu> menu. The text will start at the beginning of the path. In general "
"it is best to create an explicit path that you want the text to be fitted "
"to, rather than fitting it to some other drawing element — this will give "
"you more control without screwing over your drawing."
msgstr ""
"曲線に沿ってテキストを配置するには、テキストと曲線を一緒に選択し、「テキス"
"ト」メニューから <command>テキストをパス上に配置</command> を選びます。テキス"
"トはパスの先頭からパスに沿って表示されます。一般に、テキストを沿わせるにはそ"
"れ用にパスを作成するのが最適です。"

#. (itstool) path: sect1/title
#: tutorial-tips.xml:323
msgid "Selecting the original"
msgstr "オリジナルを選択する"

#. (itstool) path: sect1/para
#: tutorial-tips.xml:324
#, fuzzy
msgid ""
"When you have a text on path, a linked offset, or a clone, their source "
"object/path may be difficult to select because it may be directly "
"underneath, or made invisible and/or locked. The magic key <keycombo><keycap "
"function=\"shift\">Shift</keycap><keycap>D</keycap></keycombo> will help "
"you; select the text, linked offset, or clone, and press <keycombo><keycap "
"function=\"shift\">Shift</keycap><keycap>D</keycap></keycombo> to move "
"selection to the corresponding path, offset source, or clone original."
msgstr ""
"パス上に配置したテキスト、リンクオフセット、あるいはクローンがある場合、場合"
"によってはそれらのもととなるオブジェクト/パスを探すのが難しくなるかもしれませ"
"ん。なぜなら、それは直下にあるとか、見えなくしている、あるいはロックされてい"
"るかもしれないからです。マジックキー <keycap>Shift+D</keycap> がそれを助けて"
"くれます。テキスト、リンクオフセット、あるいはクローンを選択し、"
"<keycap>Shift+D</keycap> を押すと対応するパス、オフセットソース、あるいはク"
"ローンオリジナルが選択されます。"

#. (itstool) path: sect1/title
#: tutorial-tips.xml:334
msgid "Window off-screen recovery"
msgstr "画面の外に出てしまったウィンドウの回復"

#. (itstool) path: sect1/para
#: tutorial-tips.xml:335
#, fuzzy
msgid ""
"When moving documents between systems with different resolutions or number "
"of displays, you may find Inkscape has saved a window position that places "
"the window out of reach on your screen. Simply maximise the window (which "
"will bring it back into view, use the task bar), save and reload. You can "
"avoid this altogether by unchecking the global option to save window "
"geometry (<guimenuitem>Inkscape Preferences</guimenuitem>, "
"<menuchoice><guimenu>Interface</guimenu><guimenuitem>Windows</guimenuitem></"
"menuchoice> section)."
msgstr ""
"ドキュメントを解像度のやディスプレイの数が異なるシステムに移動した場合、"
"Inkscape がウィンドウの位置を記憶しているために、画面の外にウィンドウが表示さ"
"れてしまうかもしれません。その時は (タスクバーなどを使って) ウィンドウを全画"
"面化して保存し、再び開いてください。<command>Inkscape の設定</command> の "
"<command>ウィンドウ</command> タブでウィンドウの位置とサイズの保存をしないよ"
"うに設定すれば、この問題を回避できます。"

#. (itstool) path: sect1/title
#: tutorial-tips.xml:344
msgid "Transparency, gradients, and PostScript export"
msgstr "透明、グラデーション、そして PostScript エクスポート"

#. (itstool) path: sect1/para
#: tutorial-tips.xml:345
#, fuzzy
msgid ""
"PostScript or EPS formats do not support <emphasis>transparency</emphasis>, "
"so you should never use it if you are going to export to PS/EPS. In the case "
"of flat transparency which overlays flat color, it's easy to fix it: Select "
"one of the transparent objects; switch to the Dropper tool (<keycap>F7</"
"keycap> or <keycap>D</keycap>); make sure that the <guilabel>Opacity: Pick</"
"guilabel> button in the dropper tool's tool bar is deactivated; click on "
"that same object. That will pick the visible color and assign it back to the "
"object, but this time without transparency. Repeat for all transparent "
"objects. If your transparent object overlays several flat color areas, you "
"will need to break it correspondingly into pieces and apply this procedure "
"to each piece. Note that the dropper tool does not change the opacity value "
"of the object, but only the alpha value of its fill or stroke color, so make "
"sure that every object's opacity value is set to 100% before you start out."
msgstr ""
"PostScript や EPS 形式は<emphasis>透明度</emphasis>をサポートしていません。な"
"ので、それを決して PS/EPS にエクスポートしないでください。均一な透明データが"
"単一色を覆っている場合は簡単に解決できます。透明オブジェクトを選択し、スポイ"
"トツールに切り替え (<keycap>F7</keycap>)、「採取」(不透明度の採取モード) をオ"
"フにして、同じオブジェクトをクリックします。表示色が採取され、オブジェクトの"
"背後に割り当てられますが、この時点で透明度は存在しません。すべての透明オブ"
"ジェクトでこれを繰り返します。透明オブジェクトがいくつかの色の領域を覆ってい"
"る場合、それに応じて透明オブジェクトを分割し、それぞれにこの手順を適用しま"
"す。"

#. (itstool) path: sect1/title
#: tutorial-tips.xml:358
msgid "Interactivity"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-tips.xml:359
msgid ""
"Most SVG elements can be tweaked to react to user input (usually this will "
"only work if the SVG is displayed in a web browser)."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-tips.xml:363
msgid ""
"The simplest possibility is to add a clickable link to objects. For this "
"<mousebutton role=\"right-click\">right-click</mousebutton> the object and "
"select <menuchoice><guimenuitem>Create Link</guimenuitem></menuchoice> from "
"the context menu. The \"Object attributes\" dialog will open, where you can "
"set the target of the link using the value of <guilabel>href</guilabel>."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-tips.xml:369
msgid ""
"More control is possible using the interactivity attributes accessible from "
"the \"Object Properties\" dialog (<keycombo><keycap "
"function=\"control\">Ctrl</keycap><keycap function=\"shift\">Shift</"
"keycap><keycap>O</keycap></keycombo>). Here you can implement arbitrary "
"functionality using JavaScript. Some basic examples:"
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-tips.xml:376
msgid "Open another file in the current window when clicking on the object:"
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-tips.xml:381
msgid ""
"Set <guilabel>onclick</guilabel> to <code>window.location='file2.svg';</code>"
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-tips.xml:388
msgid "Open an arbitrary weblink in new window when clicking on the object:"
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-tips.xml:393
msgid ""
"Set <guilabel>onclick</guilabel> to <code>window.open(\"https://inkscape."
"org\",\"_blank\");</code>"
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-tips.xml:400
msgid "Reduce transparency of the object while hovering:"
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-tips.xml:405
msgid ""
"Set <guilabel>onmouseover</guilabel> to <code>style.opacity = 0.5;</code>"
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-tips.xml:410
msgid "Set <guilabel>onmouseout</guilabel> to <code>style.opacity = 1;</code>"
msgstr ""

#. (itstool) path: Work/format
#: tips-f01.svg:49 tips-f02.svg:49 tips-f03.svg:49 tips-f04.svg:70
#: tips-f05.svg:558 tips-f06.svg:136
msgid "image/svg+xml"
msgstr "image/svg+xml"

#~ msgid "How to do slicing (multiple rectangular export areas)?"
#~ msgstr "スライス (領域を複数の矩形に分けてエクスポート) するには"

#, fuzzy
#~ msgid ""
#~ "Create a new layer, in that layer create invisible rectangles covering "
#~ "parts of your image. Make sure your document uses the px unit (default), "
#~ "turn on grid and snap the rects to the grid so that each one spans a "
#~ "whole number of px units. Assign meaningful ids to the rects, and export "
#~ "each one to its own file (<menuchoice><guimenu>File</"
#~ "guimenu><guimenuitem>Export PNG Image</guimenuitem></menuchoice> "
#~ "(<keycombo><keycap function=\"shift\">Shift</keycap><keycap "
#~ "function=\"control\">Ctrl</keycap><keycap>E</keycap></keycombo>)). Then "
#~ "the rects will remember their export filenames. After that, it's very "
#~ "easy to re-export some of the rects: switch to the export layer, use "
#~ "<keycap>Tab</keycap> to select the one you need (or use Find by id), and "
#~ "click <guibutton>Export</guibutton> in the dialog. Or, you can write a "
#~ "shell script or batch file to export all of your areas, with a command "
#~ "like:"
#~ msgstr ""
#~ "新しくレイヤーを作成し、そのレイヤーにイメージの一部を覆うように見えない矩"
#~ "形を作成します。ドキュメントの単位が px (デフォルト) になっていることを確"
#~ "認し、グリッドをオンにして px 単位数全体に渡るように矩形をそれぞれグリッド"
#~ "にスナップさせます。各矩形に覚えやすい ID を割り当て、それぞれをそれぞれの"
#~ "ファイルへエクスポートします (<command>ファイル &gt; ビットマップにエクス"
#~ "ポート</command>〔<keycap>Shift+Ctrl+E</keycap>〕)。矩形はそれらがエクス"
#~ "ポートされたファイル名を覚えていますので、その後は矩形のいくつかは簡単に再"
#~ "エクスポートできます — エクスポートレイヤーに切り替え、Tab で (または ID "
#~ "で検索して) エクスポートしたいオブジェクトを選択し、ダイアログからエクス"
#~ "ポートをクリックします。または、以下のような、すべての領域を各領域ごとにエ"
#~ "クスポートするコマンドでシェルスクリプトやバッチファイルを書くこともできま"
#~ "す:"

#, fuzzy
#~ msgid "<command>inkscape -i area-id -t filename.svg</command>"
#~ msgstr "inkscape -i area-id -t filename.svg"

#, fuzzy
#~ msgid ""
#~ "for each exported area. The <command>-t</command> switch tells it to use "
#~ "the remembered filename hint, otherwise you can provide the export "
#~ "filename with the <command>-e</command> switch. Alternatively, you can "
#~ "use the <menuchoice><guimenu>Extensions</guimenu><guisubmenu>Web</"
#~ "guisubmenu><guimenuitem>Slicer</guimenuitem></menuchoice> extensions, or "
#~ "<menuchoice><guimenu>Extensions</guimenu><guisubmenu>Export</"
#~ "guisubmenu><guimenuitem>Guillotine</guimenuitem></menuchoice> for similar "
#~ "results."
#~ msgstr ""
#~ "-t スイッチは記憶しているファイル名ヒントを使用することを意味し、その他の"
#~ "場合は -e スイッチでファイル名を指定できます。"

#, fuzzy
#~ msgid "Click with the right mouse button"
#~ msgstr "マウスの左ボタンでダブルクリックする"

#, fuzzy
#~ msgid "Select the Pen tool from the toolbar"
#~ msgstr "ペンツールを再度選択する"

#~ msgid ""
#~ "Exporting <emphasis>gradients</emphasis> to PS or EPS does not work for "
#~ "text (unless text is converted to path) or for stroke paint. Also, since "
#~ "transparency is lost on PS or EPS export, you can't use e.g. a gradient "
#~ "from an <emphasis>opaque</emphasis> blue to <emphasis>transparent</"
#~ "emphasis> blue; as a workaround, replace it by a gradient from "
#~ "<emphasis>opaque</emphasis> blue to <emphasis>opaque</emphasis> "
#~ "background color."
#~ msgstr ""
#~ "グラデーションの PS/EPS へのエクスポートはテキスト (テキストがパスに変換さ"
#~ "れている場合を除く) やストロークの塗りに対しては機能しません。また、透明度"
#~ "は PS/EPS へのエクスポートでは失われるので、例えば<emphasis>不透明</"
#~ "emphasis>な青から <emphasis>透明</emphasis>な青へのグラデーションなどは使"
#~ "用できません。次善の策として、グラデーションを<emphasis>不透明</emphasis>"
#~ "な青から<emphasis>不透明な</emphasis>背景色に置き換えてください。"
